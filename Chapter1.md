# Computer System      

## Basic Element

**System Bus** connects **CPU**, **Main Memory**, and **I/O**      

1. CUP: control the operation of the computer and perform the data processing function
2. Main Memory: store data and program and it is volatile
3. I/O: move data between internal and external environment
4. System Bus: provide for communication

## Microprocessor

1. Microprocessor
    - The invention of the microprocessor has brought about desktop and handheld computing
    - Contain a processor on a single chip
    - Fastest general purpose processor
2. Multiprocessor
    - Each chip(socket) contains multiple processor(cores) 
3. GPU
    - provide efficient computation on arrays of data using Single-Instruction Multiple Data(SIMD) techniques pioneered in supercomputer
    - It is no longer used just for rendering advanced graphic, but it is used for general numeric processing        
4. Digital Signal Processors
    - deal with streaming signal
    - used to be embedded in I/O devices
    - encoding and decoding speech and video
    - provide support for encryption and security
5. System on a Chip
    - to satisfy the requirements of handheld devices

## Instruction Execution

1. Fetch instruction
2. Execute instruction

## Instruction Register

Processor interprets the instruction and performs required action
- processor-memory
- processor-I/O
- data processing
- control

























